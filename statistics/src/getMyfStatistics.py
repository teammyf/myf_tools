
#!/usr/bin/python3.6
# -*- coding: utf-8 -*-

'''
Get MYF server statistics
'''
#import pandas as pd
import pyodbc
import struct



def openDatabase():
    server = 'tcp:moveyourfood-sql-srv.database.windows.net'
    database = 'moveyourfood_sql_db'
    username = 'myfadmin'
    password = ''

    connectionString = 'DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password
    cnxn = pyodbc.connect(connectionString)
    cnxn.add_output_converter(-155, handle_datetimeoffset)
    #cnxn.setencoding('utf-8')  # (Python 3.x syntax)
    return cnxn


def handle_datetimeoffset(dto_value):
    # ref: https://github.com/mkleehammer/pyodbc/issues/134#issuecomment-281739794
    tup = struct.unpack("<6hI2h", dto_value)  # e.g., (2017, 3, 16, 10, 35, 18, 0, -6, 0)
    tweaked = [tup[i] // 100 if i == 6 else tup[i] for i in range(len(tup))]
    return "{:04d}-{:02d}-{:02d} {:02d}:{:02d}:{:02d}.{:07d} {:+03d}:{:02d}".format(*tweaked)


def dumpSqlServerVersion(cursor):
    cursor.execute("SELECT @@version;")
    row = cursor.fetchone() 
    while row: 
        print(row[0])
        row = cursor.fetchone()


def dumpSqlTables(cursor):
    for row in cursor.tables():
        print(row.table_name)

def getPercentRatio(val1, val2):
    retString = " / 0%"
    ratio = 0.0

    if (val2 > 0):
        ratio = val1 / float(val2) * 100
        retString = " / " + str('%.2f'%(ratio)) + "%" 

    return retString


def getSqlTabledef(tableName):
    tsql = ""

    if (tableName == "MyfDeviceRegs"):
        tsql = "SELECT id, createdAt, updatedAt, deleted, uniq_user_id, pushtoken, platform, locale, appVersion FROM " +  tableName + ";"
    
    if (tableName == "MyfHouseholds"):
        tsql = "SELECT id, createdAt, updatedAt, deleted, uniq_user_id, household_name FROM " +  tableName + ";"

    if (tableName == "MyfStorageRooms"):
        tsql = "SELECT id, createdAt, updatedAt, deleted, household_id, uniq_user_id, altName, amount, ecological, listPosition, name, numberOf, position, res, state, thawed, unit, extraProperties, list_uuid, categoryPos, itemDbRef, customUuid, inputMethod  FROM " +  tableName + ";"

    if (tableName == "MyfCustomBarcode"):
        tsql = "SELECT id, createdAt, updatedAt, deleted, uniq_user_id, description, extraIcons, quantity, household_id, quantityUnit, extraProperties, inputMethod, quantityStepsize, categoryPos FROM " +  tableName + ";"

    if (tableName == "MyfShoppingLists"):
        tsql = "SELECT id, createdAt, updatedAt, deleted, uniq_user_id, household_id, name, alias, shopped, permission  FROM " +  tableName + ";"

    if (tableName == "MyfUserInfo"):
        tsql = "SELECT id, createdAt, updatedAt, deleted, identity_provider, user_id, uniq_user_id, date_of_birth, first_name, last_name, user_locale, user_email, user_gender, household_id, hhIndex, pushConfig, badgecount  FROM " +  tableName + ";"

    if (tableName == "MyfUserRemove"):
        tsql = "SELECT id, createdAt, updatedAt, deleted, uniq_user_id FROM " +  tableName + ";"


    return tsql



def dumpSqlTable(cursor, tableName):
    tsql = getSqlTabledef(tableName)

    if (tsql != ""):
        with cursor.execute(tsql):
            rows = cursor.fetchone()
            while rows:
                #print (str(row[0]) + " " + str(row[1]))
                #print (str(row[0]))
                print (rows)
                rows = cursor.fetchone()



def showUserInfoStatistics(cursor):
    appleUsers = 0
    facebookUsers = 0
    googleUsers = 0
    usersInHousehold = 0
    loggedInLast30Days = 0
    totalUsers = 0

    tsql = getSqlTabledef("MyfUserInfo")
    with cursor.execute(tsql):
        rows = cursor.fetchone()
        while rows:
            if (rows.identity_provider == "apple"):
                appleUsers += 1    

            if (rows.identity_provider == "facebook"):
                facebookUsers += 1    

            if (rows.identity_provider == "google"):
                googleUsers += 1    

            if (rows.household_id != ""):
                usersInHousehold += 1    

            totalUsers += 1
            rows = cursor.fetchone()


    # print statistics info
    print("User statistics:\n-------------------")
    print(str(appleUsers) + getPercentRatio(appleUsers, totalUsers) + " Apple users")
    print(str(facebookUsers) + getPercentRatio(facebookUsers, totalUsers) + " Facebook users")
    print(str(googleUsers) + getPercentRatio(googleUsers, totalUsers) + " Google users")
    print(str(usersInHousehold) + getPercentRatio(usersInHousehold, totalUsers) + " Users in a household")
    print(str(totalUsers) + " Total users in database")
    print("")



def showHouseholdInfoStatistics(cursor):
    totalHouseholds = 0
    totalActiveHouseholds = 0

    tsql = getSqlTabledef("MyfHouseholds")
    with cursor.execute(tsql):
        rows = cursor.fetchone()
        while rows:
            if (rows.deleted == False):
                totalActiveHouseholds += 1
            
            totalHouseholds += 1
            rows = cursor.fetchone()


    # print statistics info
    print("Household statistics:\n-------------------")
    print(str(totalActiveHouseholds) + " Total active households")
    print(str(totalHouseholds) + " Total households (active and deleted)")
    print("")    


def showShoppingListStatistics(cursor):
    totalShoppinglists = 0
    totalActiveShoppinglists = 0

    tsql = getSqlTabledef("MyfShoppingLists")
    with cursor.execute(tsql):
        rows = cursor.fetchone()
        while rows:
            if (rows.deleted == False):
                totalActiveShoppinglists += 1
            
            totalShoppinglists += 1
            rows = cursor.fetchone()


    # print statistics info
    print("Shoppinglist statistics:\n-------------------")
    print(str(totalActiveShoppinglists) + " Total active shoppinglists")
    print(str(totalShoppinglists) + " Total shoppinglists (active and deleted)")
    print("")    


def showStorageroomStatistics(cursor):
    totalStorageroomItems = 0
    totalActiveStorageroomItems = 0
    totalStorageroomItemsInHousehold = 0
    inputMethodSearch = 0
    inputMethodBarcode = 0
    inputMethodTop50 = 0
    inputMethodInspiration = 0
    inputMethodVoice = 0

    totalItemsNotMarked = 0
    organicItems = 0
    sugarfreeItems = 0
    glutenFreeItems = 0
    lactoseFreeItems = 0
    frozenItems = 0
    veganItems = 0

    tsql = getSqlTabledef("MyfStorageRooms")
    with cursor.execute(tsql):
        rows = cursor.fetchone()
        while rows:
            if (rows.deleted == False):
                totalActiveStorageroomItems += 1
            
            if (rows.household_id != ""):
                totalStorageroomItemsInHousehold += 1

            # decode input method            
            if ((rows.inputMethod == 1) or (rows.inputMethod == 0)):
                inputMethodSearch += 1
            
            if (rows.inputMethod == 2):
                inputMethodBarcode += 1
            
            if (rows.inputMethod == 3):
                inputMethodInspiration += 1

            if (rows.inputMethod == 4):
                inputMethodTop50 += 1
            
            if (rows.inputMethod == 5):
                inputMethodVoice += 1
            
            #frozen 1, gluten 2, laktose 4, sukker 8, organic 16, Vegansk 32

            extraProperties = int(rows.extraProperties)
            # decode icons set on the items
            if (extraProperties & 16):
                organicItems += 1
            
            if (extraProperties & 8):
                sugarfreeItems += 1
            
            if (extraProperties & 2):
                glutenFreeItems += 1
            
            if (extraProperties & 4):
                lactoseFreeItems += 1
            
            if (extraProperties & 1):
                frozenItems += 1
            
            if (extraProperties & 32):
                veganItems += 1

            totalStorageroomItems += 1
            rows = cursor.fetchone()


    totalItemsNotMarked = totalStorageroomItems - (organicItems + sugarfreeItems + glutenFreeItems + lactoseFreeItems + frozenItems + veganItems)

    # print statistics info
    print("Storageroom items statistics:\n-------------------")
    print(str(totalActiveStorageroomItems) + " Total active items")
    print(str(totalStorageroomItems) + " Total items (active and deleted)")
    print(str(totalStorageroomItemsInHousehold) + getPercentRatio(totalStorageroomItemsInHousehold, totalStorageroomItems) + " Total items in a household")
    print("")

    print("Input method info:")
    print(str(inputMethodSearch) + getPercentRatio(inputMethodSearch, totalStorageroomItems) + " Total items input from search")
    print(str(inputMethodBarcode) + getPercentRatio(inputMethodBarcode, totalStorageroomItems) + " Total items input from barcode")
    print(str(inputMethodTop50) + getPercentRatio(inputMethodTop50, totalStorageroomItems) + " Total items input from top50")
    print(str(inputMethodInspiration) + getPercentRatio(inputMethodInspiration, totalStorageroomItems) + " Total items input from inspiration")
    print(str(inputMethodVoice) + getPercentRatio(inputMethodVoice, totalStorageroomItems) + " Total items input from voice")
    print("")

    print("Item marking info:")
    print(str(totalItemsNotMarked) + getPercentRatio(totalItemsNotMarked, totalStorageroomItems) + " Total items not marked")
    print(str(organicItems) + getPercentRatio(organicItems, totalStorageroomItems) + " Total items organic")
    print(str(sugarfreeItems) + getPercentRatio(sugarfreeItems, totalStorageroomItems) + " Total items sugar free")
    print(str(glutenFreeItems) + getPercentRatio(glutenFreeItems, totalStorageroomItems) + " Total items gluten free")
    print(str(lactoseFreeItems) + getPercentRatio(lactoseFreeItems, totalStorageroomItems) + " Total items lactose free")
    print(str(frozenItems) + getPercentRatio(frozenItems, totalStorageroomItems) + " Total items frozen")
    print(str(veganItems) + getPercentRatio(veganItems, totalStorageroomItems) + " Total items vegan")
    print("") 


def showCustomItemsStatistics(cursor):
    totalCustomItems = 0
    totalCustomItemsInHousehold = 0

    totalItemsNotMarked = 0
    organicItems = 0
    sugarfreeItems = 0
    glutenFreeItems = 0
    lactoseFreeItems = 0
    frozenItems = 0
    veganItems = 0

    tsql = getSqlTabledef("MyfCustomBarcode")
    with cursor.execute(tsql):
        rows = cursor.fetchone()
        while rows:
            if (rows.household_id != ""):
                totalCustomItemsInHousehold += 1
            
            extraProperties = int(rows.extraProperties)
            # decode icons set on the items
            if (extraProperties & 16):
                organicItems += 1
            
            if (extraProperties & 8):
                sugarfreeItems += 1
            
            if (extraProperties & 2):
                glutenFreeItems += 1
            
            if (extraProperties & 4):
                lactoseFreeItems += 1
            
            if (extraProperties & 1):
                frozenItems += 1
            
            if (extraProperties & 32):
                veganItems += 1

            totalCustomItems += 1
            rows = cursor.fetchone()


    totalItemsNotMarked = totalCustomItems - (organicItems + sugarfreeItems + glutenFreeItems + lactoseFreeItems + frozenItems + veganItems)

    # print statistics info
    print("Custom items statistics:\n-------------------")
    print(str(totalCustomItems) + " Total custom items")
    print(str(totalCustomItemsInHousehold) + getPercentRatio(totalCustomItemsInHousehold, totalCustomItems) + " Total custom items in a household")
    print("")  

    print("Item marking info:")
    print(str(totalItemsNotMarked) + getPercentRatio(totalItemsNotMarked, totalCustomItems) + " Total items not marked")
    print(str(organicItems) + getPercentRatio(organicItems, totalCustomItems) + " Total items organic")
    print(str(sugarfreeItems) + getPercentRatio(sugarfreeItems, totalCustomItems) + " Total items sugar free")
    print(str(glutenFreeItems) + getPercentRatio(glutenFreeItems, totalCustomItems) + " Total items gluten free")
    print(str(lactoseFreeItems) + getPercentRatio(lactoseFreeItems, totalCustomItems) + " Total items lactose free")
    print(str(frozenItems) + getPercentRatio(frozenItems, totalCustomItems) + " Total items frozen")
    print(str(veganItems) + getPercentRatio(veganItems, totalCustomItems) + " Total items vegan")
    print("")  


def showDeviceRegistrationStatistics(cursor):
    totalDeviceRegs = 0
    appVersionDict = {}

    tsql = getSqlTabledef("MyfDeviceRegs")
    with cursor.execute(tsql):
        rows = cursor.fetchone()
        while rows:
            
            if rows.appVersion in appVersionDict:
                appVersionDict[rows.appVersion] += 1
            else:
                appVersionDict[rows.appVersion] = 1

            totalDeviceRegs += 1
            rows = cursor.fetchone()


    # print statistics info
    print("Device registration statistics:\n-------------------")
    print(str(totalDeviceRegs) + " Total devices registered")
    
    print("App version info:")
    print("")

    releasedVersions = ["1.0.153", "1.0.158", "1.0.179"]

    for key, val in sorted(appVersionDict.items()):
        released = ""
        if (key in releasedVersions):
            released = " R"
            
        print(key, ":", val,getPercentRatio(val, totalDeviceRegs) + released)

    print("") 


def mainFunction():
    cnxn = openDatabase()
    cursor = cnxn.cursor()

    #print("Pyodbc version: " + pyodbc.version)
    #dumpSqlServerVersion(cursor)
    
    showUserInfoStatistics(cursor)
    showHouseholdInfoStatistics(cursor)
    showShoppingListStatistics(cursor)
    showStorageroomStatistics(cursor)
    showCustomItemsStatistics(cursor)
    showDeviceRegistrationStatistics(cursor)

    #dumpSqlTables(cursor)
    #dumpSqlTable(cursor, "MyfCustomBarcode")
    #dumpSqlTable(cursor, "MyfDeviceRegs")
    #dumpSqlTable(cursor, "MyfHouseholds")
    #dumpSqlTable(cursor, "MyfShoppingLists")
    #dumpSqlTable(cursor, "MyfStorageRooms")
    #dumpSqlTable(cursor, "MyfUserInfo")
    #dumpSqlTable(cursor, "MyfUserRemove")
    
    cursor.close()
    cnxn.close()


mainFunction()