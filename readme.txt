https://github.com/mkleehammer/pyodbc

# create virtual env
python3 -m venv ./venv --copies
source ./venv/bin/activate

on mac:
brew install unixodbc
brew install msodbcsql17 mssql-tools   (https://docs.microsoft.com/en-us/sql/connect/odbc/linux-mac/install-microsoft-odbc-driver-sql-server-macos?view=sql-server-ver15)

pip install pyodbc


# show installed packages
pip list

pip freeze > require.txt



GUI stuff
pip install PyQt5==5.9.2

guide
https://www.microsoft.com/en-us/sql-server/developer-get-started/python/mac/
https://build-system.fman.io/pyqt5-tutorial


# other user
git checkout project
git clone <project>
> cd test-project/
> python3 -m venv venv/
> pip install -r require.txt
